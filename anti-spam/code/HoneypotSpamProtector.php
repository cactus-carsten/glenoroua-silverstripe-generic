<?php

class HoneypotSpamProtector implements SpamProtector {

	public function getFormField($name = null, $title = null, $value = null) {
		return new HoneypotSpamProtectorField($name, $title, $value);
	}

	public function setFieldMapping($fieldMapping) {}

}