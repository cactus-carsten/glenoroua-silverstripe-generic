<?php

define('MODULE_GENERIC_PATH', basename(dirname(__FILE__)));

// log errors and warnings
if(!defined('SS_ERROR_EMAIL_DISABLE')) {
	SS_Log::add_writer(new CustomLogEmailWriter('carsten@cactus.net.nz'), SS_Log::WARN, '<=');
}