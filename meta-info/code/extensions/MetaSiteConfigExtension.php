<?php

class MetaSiteConfigExtension extends DataExtension {

	private static $has_one = array(
		'MetaImage' => 'Image'
	);

    public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Meta', new UploadField('MetaImage', 'Default Meta Image'));
    }
}
